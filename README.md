# Newsletter Archive

St. John's United Church of Christ in Jackson, Michigan publishes a monthly newsletter. To receive the newsletter by email, [subscribe to our newsletter](https://stjohnsjx.com/) on our website. If email is not convenient for you, you may also [request a snail mail copy](https://stjohnsjx.com/contact/) by calling us during business hours.

Here you can download previous editions of our newsletter.